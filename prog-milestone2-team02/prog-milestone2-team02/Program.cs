﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_team02
{
    class Program
    {
        static string mainmenu = "m";

        static Dictionary<string, int> dict = new Dictionary<string, int>();

        static void Main(string[] args)
        {
            //***************************
            //*							*
            //*	    	 MENU			*
            //*    						*
            //***************************
            int menu = 5;

            do
            {
                Console.Clear();

                Console.WriteLine($"               MAIN MENU                ");
                Console.WriteLine($"****************************************");
                Console.WriteLine($"*                                      *");
                Console.WriteLine($"*   1. Option 01 - Your age in days    *");
                Console.WriteLine($"*   2. Option 02 - Grade Calculator    *");
                Console.WriteLine($"*   3. Option 03 - Random Number Game  *");
                Console.WriteLine($"*   4. Option 04 - Rate your food      *");
                Console.WriteLine($"*                                      *");
                Console.WriteLine($"****************************************");
                Console.WriteLine($"        Please select an option.        ");

                menu = int.Parse(Console.ReadLine());

                if (menu == 1)
                {
                    Console.Clear();
                    option01();

                    Console.WriteLine($"Please press \"m\" to return to the main menu");
                    mainmenu = (Console.ReadLine());
                }
                if (menu == 2)
                {
                    Console.Clear();
                    option02();

                    Console.WriteLine($"Please press \"m\" to return to the main menu");
                    mainmenu = (Console.ReadLine());
                }
                if (menu == 3)
                {
                    Console.Clear();
                    play();

                    Console.WriteLine($"Would you like to return to the main menu? Please press \"m\" again to confirm.");
                    mainmenu = (Console.ReadLine());
                }
                if (menu == 4)
                {
                    Console.Clear();
                    option04();

                    Console.WriteLine($"Please press \"m\" to return to the main menu");
                    mainmenu = (Console.ReadLine());
                }
            }
            while (mainmenu == "m");
        }


        //***************************
        //*							*
        //*	    OPTION01 Danni		*
        //*							*
        //***************************

        static void option01()
        {
            DateTime current = DateTime.Now;

            Console.WriteLine("This program will tell you how many days old you are.");
            Console.WriteLine();

            Console.WriteLine("The current date and time is..");
            Console.WriteLine(current);

            Console.WriteLine();

            Console.WriteLine("Please enter your date of birth in the format YYYY/MM/DD");
            var dob = DateTime.Parse(Console.ReadLine());

            TimeSpan elapsed = current.Subtract(dob);
            double daysAgo = elapsed.TotalDays;

            Console.WriteLine();
            Console.WriteLine($"{dob} was {daysAgo.ToString("0")} days ago, you are {daysAgo.ToString("0")} days old.");

            Console.WriteLine();
            Console.WriteLine("__________________________________________________________________");
            Console.WriteLine();

            Console.WriteLine("Please enter a number to find out how many days there are in a certain amount of years.");
            var num = int.Parse(Console.ReadLine());

            var equation = num * 365.25;
            var output = Math.Floor(equation);

            Console.WriteLine($"There are {output} days in {num} years.");

            Console.WriteLine();
        }

        //***************************
        //*							*
        //*	   OPTION02 Suzanne		*
        //*							*
        //***************************

        static void option02()
        {
            var papers = new List<string>();
            var marks = new List<int>();

            Console.WriteLine("Please tell me your student ID number");
            string studentnumber = Console.ReadLine();
            var menu = "x";

            do
            {
                Console.WriteLine("Please type 5 or 6 to tell me your level of study");
                int level = Convert.ToInt32(Console.ReadLine());

                if (level == 5)

                {
                    Console.WriteLine("You will need to enter 4 paper codes");
                    Console.WriteLine("Please enter your first paper code");
                    papers.Add(Console.ReadLine());
                    Console.WriteLine("And what was your mark for this paper?");
                    int mark1 = Convert.ToInt32(Console.ReadLine());
                    marks.Add(mark1);
                    Console.WriteLine("Please enter your second paper code");
                    papers.Add(Console.ReadLine());
                    Console.WriteLine("And what was your mark for this paper?");
                    int mark2 = Convert.ToInt32(Console.ReadLine());
                    marks.Add(mark2);
                    Console.WriteLine("Please enter your third paper code");
                    papers.Add(Console.ReadLine());
                    Console.WriteLine("And what was your mark for this paper?");
                    int mark3 = Convert.ToInt32(Console.ReadLine());
                    marks.Add(mark3);
                    Console.WriteLine("Please enter your fourth paper code");
                    papers.Add(Console.ReadLine());
                    Console.WriteLine("And what was your mark for this paper?");
                    int mark4 = Convert.ToInt32(Console.ReadLine());
                    marks.Add(mark3);
                    menu = "y";
                }

                else if (level == 6)

                {
                    Console.WriteLine("You will need to enter 3 paper codes");
                    Console.WriteLine("Please enter your first paper code");
                    papers.Add(Console.ReadLine());
                    Console.WriteLine("And what was your mark for this paper?");
                    int mark1 = Convert.ToInt32(Console.ReadLine());
                    marks.Add(mark1);
                    Console.WriteLine("Please enter your second paper code");
                    papers.Add(Console.ReadLine());
                    Console.WriteLine("And what was your mark for this paper?");
                    int mark2 = Convert.ToInt32(Console.ReadLine());
                    marks.Add(mark2);
                    Console.WriteLine("Please enter your third paper code");
                    papers.Add(Console.ReadLine());
                    Console.WriteLine("And what was your mark for this paper?");
                    int mark3 = Convert.ToInt32(Console.ReadLine());
                    marks.Add(mark3);
                    menu = "y";
                }

                else
                {

                    Console.WriteLine("I'm sorry but that is an invalid number");
                    Console.WriteLine($"Press x to return to the main menu");
                    Console.ReadLine();
                    menu = "x";

                }

            } while (menu == "x");


            int sum = marks[0] + marks[1] + marks[2];
            int average = sum / 3;

            Console.WriteLine($"The total of your marks is {sum} and the average is {average}");

            Console.ReadLine();

            for (var i = 0; i < marks.Count; i++)

            {
                Console.WriteLine($"{papers[i]} = {marks[i]} = {gradeconversion(marks[i])}");
                Console.WriteLine("Please press enter");
                Console.ReadLine();
            }
        }



        public static string gradeconversion(int mark)

        {
            var grade = "";

            if (mark >= 90 && mark <= 100)
                grade = "A+";
            else if (mark >= 85 && mark <= 89)
                grade = "A";
            else if (mark >= 80 && mark <= 84)
                grade = "A-";
            else if (mark >= 75 && mark <= 79)
                grade = "B+";
            else if (mark >= 70 && mark <= 74)
                grade = "B";
            else if (mark >= 65 && mark <= 69)
                grade = "B-";
            else if (mark >= 60 && mark <= 64)
                grade = "C+";
            else if (mark >= 55 && mark <= 59)
                grade = "C";
            else if (mark >= 50 && mark <= 54)
                grade = "C-";
            else if (mark >= 40 && mark <= 49)
                grade = "D";
            else if (mark >= 0 && mark <= 39)
                grade = "E";

            return grade;
        }



        //***************************
        //*							*
        //* 	OPTION03 Jarred		*
        //*							*
        //***************************


        static int prevscores = 0;

        static List<Tuple<int, int>> userscores = new List<Tuple<int, int>>();

        static void option03()
        {
            var score = 0;
            prevscores += 1;

            int i = 0;

            for (i = 0; i <= 4; i++)

            {
                Console.WriteLine("Please enter a number between 1 and 5");

                var input = Console.ReadLine();
                int userint = 0;
                int.TryParse(input, out userint);
                Random r = new Random();
                int range = 5;
                int rInt = r.Next(1, range);

                if (userint >= 1 && userint <= 5)
                {
                    if (rInt == userint)
                    {
                        Console.WriteLine();
                        Console.WriteLine($"The random number was {rInt} and you guessed {input}. You were correct!");
                        score += 1;
                        Console.WriteLine();
                    }

                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine($"The random number was {rInt} and you guessed {input}. You were incorrect");
                        Console.WriteLine();
                    }

                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine(string.Concat("You entered ", input, " which is not an acceptable input. Please try again"));
                    if (i > 0)
                    {
                        i--;
                    }
                    Console.WriteLine();
                }

            }

            Console.WriteLine();

            Console.WriteLine($"Your score is {score} out of 5");

            userscores.Add(Tuple.Create(prevscores, score));

            play();
        }

        static void play()
        {
            Console.WriteLine("Press y if you would like to play the game");
            Console.WriteLine();
            Console.WriteLine("Press s if you would like to see your previous scores");
            Console.WriteLine();
            Console.WriteLine("Press m if you would like to end the game and go back to the main menu");
            Console.WriteLine();

            var playanswer = Console.ReadLine();

            if (playanswer == "y")
            {
                Console.Clear();
                option03();
            }
            else if (playanswer == "s")
            {
                Console.Clear();
                Console.WriteLine("These were your previous scores:");
                history();
            }
            else if (playanswer == "m")
            {
                Console.Clear();
            }
            else
            {
                Console.Clear();
                Console.WriteLine();
                Console.WriteLine("The letter you have entered is not one of the options. Please try again");
            }
            Console.WriteLine();
        }
        static void history()
        {
            foreach (var x in userscores)
            {
                Console.WriteLine($"In Game {x.Item1} your score was {x.Item2}/5");
                Console.WriteLine();
                Console.WriteLine("Press <ENTER> to continue seeing your scores");
                Console.ReadLine();
            }
        }


        //***************************
        //*							*
        //* 	OPTION04 Megan		*
        //*							*
        //***************************


        static void option04()
        {
            Console.WriteLine("Hello, please give me 5 favorite foods and rate them from 1-5 in order of favourite to least faourite.");

            addtodict();
            Console.Clear();

            checkdictcount();
            Console.WriteLine("_______________________");
            Console.WriteLine("Press Enter to continue");
            Console.WriteLine("_______________________");
            Console.ReadLine();

            Console.Clear();
            orderdict();

            Console.WriteLine("Would you like to change the order of any of your results? please press 1 for yes or 0 for no.");
            var yesorno = int.Parse(Console.ReadLine());

            if (yesorno == 1)
            {
                changeorder();
            }
            else
            {
                Console.WriteLine("Thanks for using this program!");
            }

        }
        static void addtodict()
        {


            var counter = 5;
            var i = 0;

            while (i < counter)
            {
                var a = i + 1;
                Console.WriteLine(" ______________________________________________________________________________________________________");
                Console.WriteLine(" Please enter favourite foods below:");
                Console.WriteLine("                                   ");
                Console.WriteLine($"Enter food number {a}:");
                var food = Console.ReadLine();
                Console.WriteLine($"What do you rate '{food}' out of 5?");

                var input1 = Console.ReadLine();
                int rating = 0;
                int.TryParse(input1, out rating);

                if (rating >= 1 && rating <= 5)
                {

                    dict.Add(food, rating);
                }
                else
                {
                    Console.WriteLine("Sorry that value was incorrect. You must enter a number for your rating, press enter to start over.");
                    Console.ReadLine();
                    Console.Clear();
                    addtodict();
                }
                i++;
                Console.Clear();
            }


        }
        static void checkdictcount(int count = 5)
        {

            var dictcount = dict.Count();
            Console.WriteLine($" You have entered {dictcount} food ratings");

            if (dictcount > 5)

            {
                Console.WriteLine("Sorry those entries are invalid. You need to enter 5 different foods and 5 different values, please start over");
                Console.Clear();

                dict.Clear();
                addtodict();
            }

            else
            {
                Console.WriteLine("Thankyou");
                Console.WriteLine("              ");
                Console.WriteLine("Your results are below:");
                Console.WriteLine("_______________________");
                Console.WriteLine("  Food:       Rating:");
                display_results();
            }
        }
        static void display_results()
        {
            foreach (var x in dict)
            {

                Console.WriteLine($"   {x.Key}      :         #{x.Value}");

            }


        }
        static void orderdict()
        {

            Console.WriteLine(" Here are your results in order from most to least liked favourite food");

            var items = from pair in dict
                        orderby pair.Value ascending
                        select pair;


            foreach (KeyValuePair<string, int> pair in items)
            {
                Console.WriteLine(" {0} : #{1}", pair.Key, pair.Value);
            }


        }
        static void changeorder()
        {
            Console.WriteLine("Which food would you like to change the rating of? (Please enter the food name - not the rating number)");

            var fooddict = Console.ReadLine();

            if (dict.ContainsKey(fooddict))
            {
                Console.WriteLine($" What do you rate {fooddict} out of 5?");
                var newkey = int.Parse(Console.ReadLine());

                dict[fooddict] = newkey;

                Console.WriteLine("Thanks, your new results are below");
                orderdict();

                Console.WriteLine();

            }
        }
    }


}
