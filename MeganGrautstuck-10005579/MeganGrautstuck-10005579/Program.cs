﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeganGrautstuck_10005579
{
    class Program
    {
        static Dictionary<string, int> dict = new Dictionary<string, int>();

        static void Main(string[] args)
        {
            Console.WriteLine("Hello, please give me 5 favorite foods and rate them from 1-5 in order of favourite to least faourite.");

            addtodict();
            Console.Clear();

            checkdictcount();
            Console.WriteLine("_______________________");
            Console.WriteLine("Press Enter to continue");
            Console.WriteLine("_______________________");
            Console.ReadLine();

            Console.Clear();
            orderdict();

            Console.WriteLine("Would you like to change the order of any of your results? please press 1 for yes or 0 for no.");
            var yesorno = int.Parse(Console.ReadLine());

            if (yesorno == 1)
            {
                changeorder();
            }
            else
            {
                Console.WriteLine("Thanks for using this program!");
            }

        }
            static void addtodict()
        {


                var counter = 5;
                var i = 0;

                while (i < counter)
                {
                    var a = i + 1;
                    Console.WriteLine(" ______________________________________________________________________________________________________");
                    Console.WriteLine(" Please enter favourite foods below:");
                    Console.WriteLine("                                   ");
                    Console.WriteLine($"Enter food number {a}:");
                    var food = Console.ReadLine();
                    Console.WriteLine($"What do you rate '{food}' out of 5?");
                    var rating = int.Parse(Console.ReadLine());

                    dict.Add(food, rating);

                    i++;
                    Console.Clear();
                }


        }
        static void checkdictcount(int count = 5)
        {

            var dictcount = dict.Count();
            Console.WriteLine($" You have entered {dictcount} food ratings");

            if (dictcount > 5)

            {
                Console.WriteLine("Sorry those entries are invalid. You need to enter 5 different foods and 5 different values, please start over");
                Console.Clear();

                dict.Clear();
                addtodict();
            }

            else
            {
                Console.WriteLine("Thankyou");
                Console.WriteLine("              ");
                Console.WriteLine("Your results are below:");
                Console.WriteLine("_______________________");
                Console.WriteLine("  Food:       Rating:");
                display_results();
            }
        }
        static void display_results()
        {
            foreach (var x in dict)
            {

                Console.WriteLine($"   {x.Key}      :         #{x.Value}");

            }


        }
        static void orderdict()
        {

            Console.WriteLine(" Here are your results in order from most to least liked favourite food");

            var items = from pair in dict
                        orderby pair.Value ascending
                        select pair;


            foreach (KeyValuePair<string, int> pair in items)
            {
                Console.WriteLine(" {0} : #{1}", pair.Key, pair.Value);
            }


        }
        static void changeorder()
        {

            Console.WriteLine($"Which food would you like to change the rating of ? ");



        }
    }
}
